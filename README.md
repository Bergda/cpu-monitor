### How to run in dev enviroment
Run ```npm i``` and ```npm start``` at ```/cpu-monitor``` in your terminal.

### How to build
Run ```npm run make``` for a smaller installation with a one click- installer, or ```npm run build-installer``` for a larger installation but a more customized installer. You will get an executable application and an executable installer for installing the application locally with both scripts.

### Exe file locations
#### With ```make```
For windows, the files are created into the ```out```-directory. The setup-file is located deeper under the ```make```-directory, and the executable application is located in the other folder.

### With ```build-installer```
For windows, the files are created into the ```dist```-directory. The setup-file is located in the main folder, and the executable application is located in the ```win-unpacked```-directory.